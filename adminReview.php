<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>

  <?php 
  require("/controller/sessionstart.php");
  require('../test/controller/db.php'); 
  include("navbar.php");
  ?>


  <meta charset="UTF-8">
  <title>Model Review</title>


</head>
<body>
 <div class="container">
  <h2 class="text-center txttweak">Model Review</h2>


  <?php
  $db = DBconnection();
  $req = $db->query("SELECT * FROM prints WHERE isPrinted=0 ORDER BY date ASC, priority DESC  ");
  $message=0;
  ?>
  <?php if($req->rowCount() > 0){
    $message=1;
    ?>
    <form>
      <table table id="example" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
        <thead>
          <tr> 
            <th>Select</th>
            <th>Date</th>
            <th>File</th>
            <th>Student Name</th>
            <th>Status</th>
            <th>Material</th>
            <th>Quality</th>
            <th>Color</th>
            <th>Options</th>
          </tr>
        </thead>
        <tfoot>                
          <tr> 
            <th>Select</th>
            <th>Date</th>
            <th>File</th>
            <th>Student Name</th>
            <th>Status</th>
            <th>Material</th>
            <th>Quality</th>
            <th>Color</th>
            <th>Options</th>
          </tr>
        </tfoot>
        <tbody>
          <?php 

          while($row = $req->fetch(PDO::FETCH_ASSOC)){
            $getName = $db->query("SELECT * FROM login WHERE userID='".$row['userID']."'"); 
            $theName = $getName->fetch(PDO::FETCH_ASSOC);

            if ($row['isPaid']== 1){
              if ($row['isApproved']== 0){
                $status='<td class="orange">Awaiting Approval</td>';
              }else{
                $status='<td class="green">Ready to Print</td>';
              }
            }
            else{
              if ($row['isApproved']== 0){
                $status='<td class="red">Awaiting Payment and Approval</td>';
              }else{
                $status='<td class="yellow">Ready to Print and Awaiting Payment</td>';
              }
            }

            ?>
            <tr>
              <td><div class="flex-center"><input type="checkbox" class="filled-in deep-purple accent-3" id="<?php echo $row['printID']; ?>" name="gaben[]" value="<?php echo $row['printID']; ?>">
                <label for="<?php echo $row['printID']; ?>"></label></div></td>
                <td><?php echo $row['date']; ?></td>
                <td><?php echo $row['file']; ?></td>
                <td><?php echo $theName['name'] ?> <?php echo $theName['surname'] ?></td>
                <?php

                echo $status ?>
                <td><?php echo $row['material']; ?></td>
                <td><?php echo $row['quality']; ?></td>
                <td><?php echo $row['color']; ?></td>
                <td><?php 
                if ($row['isPaid']==0){
                  if ($row['isApproved']== 1 ){
                    echo '<a type="button" class="btn btn-secondary red" onclick="uSure()" >Already REVIEWED</a></td>';
                  }else{
                   echo '<a type="button" class="btn btn-secondary" href="review.php?id='.$row['printID'].'">Review</a></td>';
                 }
               }else{
                echo '<button type="button" class="btn btn-primary grey" disabled>REVIEWED</button></td>';
               }
                
               ?>
             </tr>
             <?php 
           } 
         }
         $req->closecursor();
         $getName->closecursor();
         ?>
       </tbody>
     </table> 



   </div>
 </div>
</form>
</div> 


<!-- MODAL -->
<!-- MODAL -->
<!-- MODAL -->
<!-- MODAL -->



    
    <!-- Central Modal Medium Warning -->
    <div class="modal fade" id="uSure" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-warning" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">are you sure?</p>
    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
    
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-warning fa-4x mb-3 animated rotateIn"></i>
                        <p>This model has already been reviewed. Are you sure you want to review it again?</p>
                    </div>
                </div>
    
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a type="button" class="btn btn-primary-modal"  href="review.php?id='.$row['printID'].'">Yes<i class="fa fa-diamond ml-1"></i></a>
                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">No</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Warning-->







<script type="text/javascript">


  $(document).ready(function() {
    $('#example').DataTable({
          "bPaginate": false,
    "bLengthChange": false,
    "bFilter": true,
    "bInfo": false,
    "bAutoWidth": false} );
  });
  function uSure(){
  $("#uSure").modal('show');
}
</script>

</body>
</html>