<!DOCTYPE html>
<html>
<head>
	<title>Password recovery system</title>
	<?php
	include("header.php");
	?>
</head>

<body>

    
    <!-- Central Modal Medium Danger -->
    <div class="modal fade" id="recoveryFail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Error.</p>
    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
    
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa  fa-times-circle fa-4x mb-3 animated rotateIn"></i>
                        <p>This Email isn't in our database. please make sure you've typed it properly</p>
                    </div>
                </div>
    
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Okay</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Danger-->
    
    <?php 
    if (isset($_GET['fail']) == true) { ?>
        <script type="text/javascript">
        $(document).ready(function(){$("#recoveryFail").modal('show');});
   		 </script>
<?php } ?>	


	<form action="controller/recoveryaction.php" method="post">
		<div class="container">
			<div class="containersamere z-depth-3 white animated bounceInUp">
				<h3 class="txttweak">Password recovery system</h3>
				<div class="row"><br></div>
					<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md">
						<div class="md-form">
							<i class="fa fa-envelope prefix"></i>
							<input type="text" id="form9" name="username" >
							<label for="form9">Type your Email</label>
						</div>
					</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row"><p></p></div>
				<div class="row">
					<div class="col-md-2"></div>
					<button class="btn btn-deep-purple deep-purple accent-3 waves-light col-md" mdbRippleRadius>Send Recovery Email</button>
					<div class="col-md-2"></div>
				</div>

			</div>
		</div>
	</form>
</body>
<?php 

include("footer.php");
?>
</html>