<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
  <?php 

  require("/controller/sessionstart.php");
  require('../test/controller/db.php');     
  include("navbar.php");
  ?>
  <!--          <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
  <meta charset="UTF-8">
  <title>Current print jobs for <?php echo $_SESSION['username']; ?></title>
</head>
<body>
  <form action ="controller/current.php" method ="post" >
    <div class="container">
      <h2 class="text-center txttweak"> Current Print Jobs for <?php echo $_SESSION['name']; ?> <?php echo $_SESSION['surname']; ?></h2>
      <div class="row">
        <div class="col">
          <?php if (isset($new)) { ?>
          <p>Your stuff will get printed shortly</p> 

          <?php } 
          $db = DBconnection();
          $req = $db->query("SELECT * FROM prints WHERE userID='".$_SESSION['userID']."' AND isPrinted=0");
          $message=0;
          ?>
          <?php if($req->rowCount() > 0):
          $message=1?>
          <div class="dattab animated slideInLeft">
            <table>

              <tr>
                <th class="dattablabel">Select</th>
                <th class="dattablabel">Date</th>
                <th class="dattablabel">File</th>
                <th class="dattablabel">Material</th>
                <th class="dattablabel">Quality</th>
                <th class="dattablabel">Color</th>
                <tr>
                  <?php while($row = $req->fetch(PDO::FETCH_ASSOC)): ?>
                    <tr class="dattabcontent">
                      <td class="dattabcontent"><input type="checkbox" class="filled-in deep-purple accent-3" id="<?php echo $row['printID']; ?>" name="gaben[]" value="<?php echo $row['printID']; ?>">
                        <label for="<?php echo $row['printID']; ?>"></label></td>
                        <td class="dattabcontent"><?php echo $row['date']; ?></td>
                        <td class="dattabcontent"><?php echo $row['file']; ?></td>
                        <td class="dattabcontent"><?php echo $row['material']; ?></td>
                        <td class="dattabcontent"><?php echo $row['quality']; ?></td>
                        <td class="dattabcontent"><?php echo $row['color']; ?></td>
                      </tr>
                    <?php endwhile; 
                    $req->closecursor();?>
                  </table>
                <?php endif; ?>
                <?php if ($message==0){ ?>
                <h2 class="text-center txttweak">YOU HAVE NO JOBS</h2>
                <?php } ?>
              </div>
            </div>
            <div class="col-md animated slideInUp flex-center">
              <?php if ($message==1){ ?>

              <button class="btn btn-primary deep-purple accent-3 waves-light" mdbRippleRadius value="1">cancel print</button>
<?php } ?>
            </div>
            
            
          </div>
        </div>
      </form>
    </body>
    <?php 
    include("footer.php");
    ?>
    </html>
