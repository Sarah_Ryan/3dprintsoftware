<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
    <meta charset="UTF-8">
    <title>Change Password</title>
    <?php 
    require("controller/sessionstart.php");
    include("navbar.php");

    ?>
</head>
<body>

    <form action="controller/passChange.php" method="post">
        <div class="container">
            <div class="containersamere z-depth-3">
              <!-- Content here -->

              <h2 class="txtlogin"> Change your password </h2>
              
              <?php if(isset($_GET['fail']) == true){ ?>
              <div><p class="danger-text" style="text-align: center;">Wrong password !!!</p>

              </div>
              <?php } ?>
              <div>
                  <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md">

                        <div class="md-form">
                            <i class="fa  fa-unlock-alt prefix"></i>
                            <input type="password" id="form10" class="form-control " name="oldPass" required>
                            <label for="form10">Old password</label>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md">               
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input type="password" id="form11" class="form-control " name="newPass" required>
                            <label for="form11">New password</label>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md">               
                        <div class="md-form">
                            <i class="fa fa-lock prefix"></i>
                            <input type="password" id="form12" class="form-control " name="passCheck" required>
                            <label for="form12">Password Confirm</label>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
<!--             <div class="row">
                <div class="col-md-2"></div>     
                <div class="col-md-8 g-signin2 " data-onsuccess="onSignIn"></div>
                <div class="col-md-2"></div>
            </div> --> 
            <div class="row">
                <div class="col-md-2"></div>
                <button class="btn btn-primary waves-light deep-purple accent-3 col-md" mdbRippleRadius>Submit</button>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div> 
</div>

<?php
        // put your code<?php 
include("footer.php");

?>
</body>
</html>