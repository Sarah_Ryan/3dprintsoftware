<!DOCTYPE html>
<html>
<head>
  <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
  <meta charset="UTF-8">
  <title></title>
  <?php 
  require("/controller/sessionstart.php");
  include("navbar.php");
  include('googleWrap.php');

  $client = getClient();
  $service = new Google_Service_Sheets($client);
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
  $spreadsheetId = '1NPf1czgspzjjmkwiRBSErp8YknRbYs5zmZMP7uR8G8c';
  $range = 'A2:B';
  $response = $service->spreadsheets_values->get($spreadsheetId, $range);
  $values = $response->getValues();
  $array=array();
  $array1=array();
  if (count($values) == 0) {
  } else {
    foreach ($values as $row) {
    // Print columns A and E, which correspond to indices 0 and 4.
      /*    printf("%s, %s\n", $row[4], $row[14]);*/

      if (!in_array($row[0], $array)){
       if ($row[0]!=' '){
        $array[] = $row[0];
      }
    }
  }
  foreach ($values as $row) {
    if (!in_array($row[1], $array)){
      if ($row[1]!= ' '){
        $array1[] = $row[1];
      }
    }
  }
}
?>
</head>
<body class="grey lighten-3">

<!-- <audio loop autoplay>
  <source src="aloha.ogg" type="audio/ogg">
  </audio> -->

  <div id="mdb-preloader" class="flex-center">
    <div id="preloader-markup">
    </div>
  </div>

  <div class="container">
   <div class="datupload z-depth-3 white animated bounceInUp">
    <!-- Content here -->

    <h2 class="txttweak"> New Print Job </h2>
    
    <div class="row"><br></div> 


    <form action ="Upload.php" method ="Post" enctype ="multipart/form-data">
      <div class="row">

        <div class="col-md">

         <div class="row">         
          <div class="col-xs-1">
            <a onclick="openInfo(1)" class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
          </div>
          <div class="col"> 

            <div class="file-field">              
              <a class="btn-floating purple-gradient mt-0" title="Upload a file for review.">
                <i class="fa fa-cloud-upload" aria-hidden="true"  title="Upload a file for review."></i>
                <input type="file" Name="fileToUpload"  title="Upload a file for review.">
              </a>
              <div class="file-path-wrapper" title="Upload a file for review.">
               <input class="file-path validate" Name="fileToUpload" type="text" placeholder="Upload your file"  title="Upload a file for review." required>
             </div><label class="myLabel"  title="Upload a file for review."><?php if($_SESSION["isPowerUser"]==1){ echo '<span class="success-text">.gcode</span>'; } ?> .obj/.stl files Only!</label>
           </div>
         </div>

       </div>


     </div>
     <div class="col-md">

       <div class="row">        
        <div class="col-xs-1">
         <a class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
       </div>
       <div class="col">
        <select name="isPersonal" class="mdb-select colorful-select dropdown-primary " required>
          <option  value="" disabled selected>Choose an Answer</option>
          <option value="-1">Personal</option>
          <option value="-1">Formative</option>
          <option value="0">Summative</option>
        </select>
        <label> What type of project is it?</label>
      </div>

    </div>


  </div>

</div>

<div class="row"><br></div>

<div class="row">
  <div class="col-md">

   <div class="row">      
    <div class="col-xs-1">
      <a class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
    </div>
    <div class="col">
      <select name="isGroup" class="mdb-select colorful-select dropdown-primary " required>
        <option  value="" disabled selected>Choose an option</option>
        <option value="1">Yes</option>
        <option value="0">No</option>
      </select>
      <label>Group work?</label>
    </div>

  </div>


</div>
<div class="col-md">

 <div class="row">
  <div class="col-xs-1">
   <a class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
 </div>
 <div class="col"> 
   <select name="material" class="mdb-select colorful-select dropdown-primary " required>
    <option  value="" disabled selected>Choose your Material</option>
    <?php    
    foreach($array1 as $yatangaki) {
      echo '<option value="'.$yatangaki.'">'.$yatangaki.'</option><br>';
      echo "shit";
    }
    ?>
  </select>
  <label>Material</label>
</div>

</div>


</div>
</div>

<div class="row"><br></div>

<div class="row">
  <div class="col-md">


   <div class="row">
    <div class="col-xs-1">
      <a class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
    </div>
    <div class="col">
      <select name="quality" class="mdb-select colorful-select dropdown-primary" required>
        <option value="" disabled selected>Choose your Quality</option>
        <option value="Extra Coarse">Extra Coarse (0.6mm) </option>
        <option value="Coarse">Coarse (0.4mm) </option>
        <option value="Draft">Draft (0.2mm)</option>
        <option value="Low">Low (0.15mm) </option>
        <option value="Fine">Fine (0.1mm) </option>
        <option value="Awesome">Extra Fine (0.06mm) </option>
      </select>
      <label>Quality</label>
    </div>
    
  </div>





</div>
<div class="col-md">

 <div class="row">  
  <div class="col-xs-1">
   <a onclick="openInfo(1);" class="badge purple darken-2 hoverable" ><i class="fa fa-2x fa-question-circle" aria-hidden="true"></i></a>
 </div>
 <div class="col">
  <select name="colour" class="mdb-select colorful-select dropdown-primary " required>
    <option value="" disabled selected>Choose your Colour</option>
    <?php    
    foreach($array as $result) {
      echo '<option value="'.$result.'">'.$result.'</option><br>';
    }
    /*    exec('"C:\Program Files (x86)\VideoLAN\VLC\vlc.exe" "C:\aloha.mpeg"');*/
    /*    unlink('C:\Users\Antoine\AppData\Roaming\cura\3.0\cura.log');*/
    /*  exec('"C:\Program Files\Ultimaker Cura 3.0\Cura.exe" "C:\Users\Antoine\Desktop\Antoinehacker.obj"'); */
    ?>
  </select>
  <label>Colour</label>
</div>

</div>



</div>

</div>
<div class="row">
  <div class="col">
    <div class="md-form">
      <textarea id="textarea-char-counter" class="md-textarea" length="128"></textarea>
      <label for="textarea-char-counter">Any comments?</label>
    </div>
  </div>
</div>

<div class="row">
  <br>
  <div class="col-md-12 flex-center bitonopload">
    <button class="btn btn-primary deep-purple accent-3 waves-light col-md" mdbRippleRadius>Upload for review</button>
  </div>
</div>
</form>
</div>

<!-- modal -->
<!-- modal -->
<!-- modal -->
<!-- modal -->
<!-- modal -->

<!-- Central Modal Medium Info -->
<div class="modal fade" id="quality" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">

      <div class="modal-header">
        <p class="heading">Related article</p>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <img src="img/Quality.jpg" style="height: 100%; width: 100%;" ></img>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
<!-- Central Modal Medium Info-->



</div>
<script type="text/javascript">

  function openInfo(button) {
    if (button==1){
      $("#quality").modal('show');
    }
    if (button==2){
      $("#quality").modal('show');
    }
    if (button==3){
     $("#quality").modal('show');
   }
   if (button==4) {
     $("#quality").modal('show');
   }
   if (button==5){
     $("#quality").modal('show');
   }
   else{
     $("#quality").modal('show');
   }

 }

 $(document).ready(function() {$('.mdb-select').material_select();});

</script>
</div>
</body>
<?php
include("footer.php");
?>
</html>
