<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
  <?php 
  require("/controller/sessionstart.php");
  require('../test/controller/db.php'); 
  include("navbar.php");
  ?>
  <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
  <meta charset="UTF-8">
  <title>List of Old Jobs for <?php echo $_SESSION['username']; ?></title>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>

  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

</head>
<body>
<form action ="controller/accountsAdmin.php" method ="post" >
  <div class="container">

    <h2 class="text-center txttweak"> All User Accounts !</h2>
    <div class="row">

      <div class="col-md">
        <?php
        $db = DBconnection();
        $req = $db->query("SELECT * FROM login");
        $message=0;
        ?>

        <?php if($req->rowCount() > 0): 
        $message=1;?>
        <!--  <div class="animated slideInLeft"> -->
          <table id="table" class="table table-hover table-striped table-bordered table-responsive w-auto" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th >Select<i class="fa fa-sort ml-1"></i></th>
                <th >ID<i class="fa fa-sort ml-1"></i></th>
                <th>Name<i class="fa fa-sort ml-1"></i> </th>
                <th>Surname<i class="fa fa-sort ml-1"></i> </th>
                <th>Print priority Level<i class="fa fa-sort ml-1"></i> </th>
                <th>Locked?<i class="fa fa-sort ml-1"></i> </th>
                <tr>
                </thead>
                <tbody>
                  <?php
                  $printNum = 1; 
                  while($row = $req->fetch(PDO::FETCH_ASSOC)): ?>
                  <tr>
                      <td><input type="checkbox" class="filled-in deep-purple accent-3" id="<?php echo $row['userID']; ?>" name="<?php  echo 'gaben[]'; ?>" value="<?php echo $row['userID']; ?>"><label for="<?php echo $row['userID']; ?>"></label></td>
                      <th scope="row"><?php echo $row['userID'] ?></th>
                      <td><?php echo $row['name']; ?></td>
                      <td><?php echo $row['surname']; ?></td>
                      <td><?php echo $row['priority']; ?></td>
                      <?php if ($row['isLocked']==1){ echo "<td class='red'>Locked";}else{echo "<td class='green'>Unlocked";} ?></td>                      
                    </tr>
                    <?php
                    $printNum++;
                  endwhile; 
                  $req->closecursor();?>
                </tbody>
              </table>
              <!--               </div>  -->        
            </div>
            <div class="col-md animated slideInUp flex-center">
              <?php if ($message==1){ ?>
              <button class="btn btn-primary deep-purple accent-3 waves-light" mdbRippleRadius name="lock" value="1">Lock <i class="fa fa-lock ml-1"></i></button>
              <button class="btn btn-primary deep-purple accent-3 waves-light" mdbRippleRadius name="unlock" value="1">Unlock <i class="fa fa-unlock-alt ml-1"></i></button>
              <?php } ?>
            </div>
          <?php endif; ?>
          <?php if ($message==0){ ?>
          <h2 class="text-center txttweak">No one registered yet...</h2>
          <h1 class="text-center txttweak">:/</h2>
          <?php } 
          include("footer.php");
          ?>
        </div>
      </div>
    </form>


   <!-- Central Modal Medium Success -->
    <div class="modal fade" id="lockSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-success" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Lock Success</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-lock fa-4x mb-3 animated rotateIn"></i>
                        <p>Account(s) Locked successfully!</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Ok !</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->

<?php if(isset($_GET['locked']) == true){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#lockSuccess").modal('show');});
    </script>
<?php } ?>



   <!-- Central Modal Medium Success -->
    <div class="modal fade" id="unlockSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-success" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Unlock Success</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-unlock-alt fa-4x mb-3 animated rotateIn"></i>
                        <p>Account(s) unlocked successfully!</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Ok !</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->

<?php if(isset($_GET['unlocked']) == true){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#unlockSuccess").modal('show');});
    </script>
<?php } ?>





  </body>
  </html>
