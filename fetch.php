<?php

include('controller/db.php');
session_start();

if(isset($_POST['view'])){

// $con = mysqli_connect("localhost", "root", "", "notif");

	$db = DBconnection();

	if($_POST["view"] !=""){
		$req = $db->query('UPDATE comments SET comment_status = 1 WHERE comment_status=0');
		$req->closeCursor();
	}

	$req = $db->query("SELECT * FROM comments ORDER BY comment_id DESC LIMIT 6");
	$output = "";
	$message = array();
	$messageTitle = array();
	$id=0;
	$newMessage=0;
	if($req->rowCount() > 0){
		while($row = $req->fetch(PDO::FETCH_ASSOC)){
			if (strpos($row['destination'],$_SESSION['userID']) !== false || $row['destination'] == 'ALL' || $row['destination'] ==  'ADMIN' && $_SESSION['isAdmin']==1){
				$shortSubject = mb_substr($row["comment_subject"], 0, 25);
				$shortmsg = mb_substr($row["comment_text"], 0, 30);
				$output .= '<a class="dropdown-item" onclick="openMessage('.$id.')"><strong>'.$shortSubject.'</strong><br /><small><em>'.$shortmsg.'</em></small></a>';
				$id++;
				$message[] = $row["comment_text"];
				$messageTitle[] = $row["comment_subject"];
				if($row['comment_status']==0){
					$newMessage++;
				}
			}
		}
	}else{
		$output .= '<li><a class="dropdown-item text-bold text-italic" href="#" >You don\'t have any messages yet.</a></li>';
	}
	
	$count = $newMessage;

	$data = array('notification' => $output,'unseen_notification'=>$count,'message' => $message, 'messageTitle' => $messageTitle);
	
	echo json_encode($data);
}
?>