<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>

  <?php 
  require("/controller/sessionstart.php");
  require('../test/controller/db.php'); 
  include("navbar.php");
  ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
  <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
  <meta charset="UTF-8">
  <title>List of Old Jobs for EVERYONE</title>


</head>
<body>
 <div class="container">
  <h2 class="text-center txttweak"> Old Print Jobs for EVERYONE</h2>

  <?php
  $db = DBconnection();
  $req = $db->query("SELECT * FROM prints WHERE isPrinted=1");
  $message=0;
  ?>
  <?php if($req->rowCount() > 0){
    $message=1;
    ?>
    <form>
      <table table id="example" class="table table-striped table-bordered table-responsive" cellspacing="0" width="100%">
        <thead>
          <tr> 
            <th>Select</th>
            <th>Date</th>
            <th>Name</th>
            <th>Surname</th>
            <th>File</th>
            <th>Material</th>
            <th>Quality</th>
            <th>Color</th>
        </tr>
    </thead>
    <tfoot>                
      <tr> 
        <th>Select</th>
        <th>Date</th>
        <th>Name</th>
        <th>Surname</th>
        <th>File</th>
        <th>Material</th>
        <th>Quality</th>
        <th>Color</th>
    </tr>
</tfoot>
<tbody>
  <?php 

  while($row = $req->fetch(PDO::FETCH_ASSOC)){
    $getName = $db->query("SELECT * FROM login WHERE userID='".$row['userID']."'"); 
    $theName = $getName->fetch(PDO::FETCH_ASSOC);

    ?>
    <tr>
      <td><div class="flex-center"><input type="checkbox" class="filled-in deep-purple accent-3" id="<?php echo $row['printID']; ?>" name="gaben[]" value="<?php echo $row['printID']; ?>">
        <label for="<?php echo $row['printID']; ?>"></label></div></td>
        <td><?php echo $row['date']; ?></td>
        <td><?php echo $theName['name'] ?></td>
        <td><?php echo $theName['surname'] ?></td>
        <td><?php echo $row['file']; ?></td>
        <td><?php echo $row['material']; ?></td>
        <td><?php echo $row['quality']; ?></td>
        <td><?php echo $row['color']; ?></td>
    </tr>
    <?php 
    $getName->closecursor();
} 
}
$req->closecursor();

?>
</tbody>
</table> 
</div>
</div>

<script type="text/javascript">
   $(document).ready(function() {$('#example').DataTable();});
</script>
</form>
</div>
</body>
</html>