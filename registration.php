<!DOCTYPE html>
<!DOCTYPE html>
<html>
<head>
	<title>Registration</title>
	<?php
	include('header.php');
	include('googleWrap.php');
$client = getClient();


$service = new Google_Service_Sheets($client);

// Prints the names and majors of students in a sample spreadsheet:
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
$spreadsheetId = '1VbsL6a2xrOrewj55_gTWVcYzdutqwONAwhSPYJg-Xn0';
$range = 'A2:Q';
$response = $service->spreadsheets_values->get($spreadsheetId, $range);
$values = $response->getValues();
$array=array();
$array1=array();
if (count($values) == 0) {
	/*print "No data found.\n";*/
} else {
	/*  print "Name, Major:\n";*/
	foreach ($values as $row) {
    // Print columns A and E, which correspond to indices 0 and 4.
		/*    printf("%s, %s\n", $row[4], $row[14]);*/

		if (!in_array($row[4], $array))
		{
			$array[] = $row[4];
			$array1[] = $row[14];
		}
	}
}



?>
</head>
<body>
	<form action="controller/register.php" method="post">
		<div class="container">
			<div class="datupload z-depth-3 white animated bounceInUp">
				<div class="row">
					<div class="col-md">
						<h3 class="txttweak">Registration Form</h3>
					</div>
					
				</div>
				<div class="row"><br></div>
				<div class="row">
					
					<div class="col-md">
						<div class="md-form">
							<i class="fa fa-envelope prefix"></i>
							<input type="text" id="form9" name="username" >
							<label for="form9">Type your Email</label>
						</div>
					</div>

					<div class="col-md">
						<div class="md-form">
							<i class="fa  fa-user prefix"></i>
							<input type="text" id="form9" name="name">
							<label for="form9">Type your Name</label>
						</div>
					</div>
					
				</div>
				<div class="row"><br></div>
				<div class="row">
					
					<div class="col-md">
						<div class="md-form">
							<i class="fa fa-user prefix"></i>
							<input type="text" id="form9" name="surname">
							<label for="form9">Type your Surname</label>
						</div>
					</div>

					<div class="col-md">
						<div class="md-form">
							<i class="fa fa-lock prefix"></i>
							<input type="password" id="form9" < name="passwd">
							<label for="form9">Type your Password</label>
						</div>
					</div>
				</div>
				<div class="row"><br></div>
				<div class="row">
					
					<div class="col-md">
						<select name="courseName" class="mdb-select colorful-select dropdown-primary" required>
							<?php    
							echo '<option value="" disabled selected>Choose your course</option>';
							foreach($array as $result) {
								foreach($array1 as $datvalue) {
								}
								echo '<option value="'.$datvalue.'">'.$result.'</option><br>';
							}
							?>
						</select>
						<label>Course </label>
					</div>
					<div class="col-md">
<select name="year" class="mdb-select colorful-select dropdown-primary" required>
							<option value="" disabled selected>Choose your Year</option>
							<option value="2">year 3</option>
							<option value="1">year 2</option>
							<option value="0">year 1</option>
						</select>
						<label>Year</label>
									</div>
					
				</div>
				<div class="row"><br></div>
				<div class="row">
					<div class="col-md-2"></div>
					<button class="btn btn-deep-purple deep-purple accent-3 waves-light col-md" mdbRippleRadius>Submit</button>
					<div class="col-md-2"></div>
				</div>

				<div class="row">
					<br>
				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript">$(document).ready(function() {
		$('.mdb-select').material_select();
	});</script>
</body>
<?php 
include("footer.php");
?>
</html>
