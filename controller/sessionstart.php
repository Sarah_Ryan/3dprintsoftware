<?php
// Vérification qu'une session n'est pas déjà en cours 
session_start();
// Si il n'y a pas de session en cours renvoie vers la page de connexion
if (!empty($_SESSION['username'])) {
} else {
	header('location: index.php');
}
?>