<?php 


$loginUser = strtolower(htmlspecialchars($_POST['username']));
$loginPass = htmlspecialchars($_POST['pass']);  	
session_start();
require('db.php');

$db = DBconnection();
$reponse = $db->query('SELECT * FROM login');

while($data = $reponse->fetch()){
  if($data['username'] == $loginUser) {      
    $_SESSION['username'] = $_POST['username'];
    /*$error="1";*/
    $_SESSION['userID'] = $data['userID'];
    $_SESSION['name'] =  $data['name'];
    $_SESSION['surname'] =  $data['surname'];
    $_SESSION['isLocked'] = $data['isLocked'];
    $_SESSION['priority'] = $data['priority'];
    $_SESSION['isPowerUser'] = $data['isPowerUser'];
    $_SESSION['isAdmin'] = $data['isAdmin'];

// See the password_hash() example to see where this came from.
    if (password_verify($loginPass,$data['pass'])) {
      echo 'Password is valid!';
      if ($data['isLocked']==1){
       header("Location: ../login.php?locked=true");
     }

     if ($data['isLocked']== 0){
      $addfail = $db->query("UPDATE login SET failCount=0 WHERE userID ='".$data['userID']."'");
      $addfail->closeCursor();
      header('Location: ../Validation.php');


    }
  } else {
    echo 'Invalid password.';
    
    $failCount=$data['failCount'];
    if ($data['failCount'] < 3){
      $failCount++;  
      $addfail = $db->query("UPDATE login SET failCount='".$failCount."' WHERE userID ='".$data['userID']."'");
      $addfail->closeCursor();
      header("Location: ../login.php?wrongPass=true");   
    }
    if ($failCount == 3 && $data['isLocked'] == 1){
      header("Location: ../login.php?locked=true");
    }
    if ($failCount == 3){
      $lockDatShit = $db->query("UPDATE login SET isLocked='1' WHERE userID ='".$_SESSION['userID']."'");
      $lockDatShit->closeCursor();
      header("Location: ../login.php?locked=true");
    }
    
  }         
} else {
  echo "unhandled error";
}
}

$reponse->closeCursor();

?>