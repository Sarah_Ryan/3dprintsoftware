
<!DOCTYPE html>
<html>
<head>
	<?php
session_start();
session_destroy();
include("header.php");	
?>
	<title>Logged Out!</title>
</head>
<body>
	<div class="container">

<h4 class="txtlogin">You have been logged out.<a href="index.php">Go Back to Login.</a></h4>
<meta http-equiv="refresh" content="0; url=../login.php" />
</div>
</body>
<?php 
include("footer.php");
?>
</html>