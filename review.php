<!DOCTYPE html>
<html>
<head>
	<title>Model Review</title>
	<?php
	include('controller/sessionstart.php');	
	include('navbar.php');
	include('controller/db.php');
	include('googleWrap.php');

	$client = getClient();
	$service = new Google_Service_Sheets($client);
// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
	$spreadsheetId = '1NPf1czgspzjjmkwiRBSErp8YknRbYs5zmZMP7uR8G8c';
	$range = 'A2:B';
	$response = $service->spreadsheets_values->get($spreadsheetId, $range);
	$values = $response->getValues();
	$array=array();
	$array1=array();
	if (count($values) == 0) {
	} else {
		foreach ($values as $row) {
    // Print columns A and E, which correspond to indices 0 and 4.
			/*    printf("%s, %s\n", $row[4], $row[14]);*/

			if (!in_array($row[0], $array)){
				if ($row[0]!=' '){
					$array[] = $row[0];
				}
			}
		}
		foreach ($values as $row) {
			if (!in_array($row[1], $array)){
				if ($row[1]!= ' '){
					$array1[] = $row[1];
				}
			}
		}
	}
	?>
</head>
<body>
	<?php
	$db = DBconnection();
	$req = $db->query('SELECT * FROM prints WHERE printID='.$_GET['id']);
	$row = $req->fetch(PDO::FETCH_ASSOC);
	$getName = $db->query("SELECT * FROM login WHERE userID='".$row['userID']."'"); 
	$theName = $getName->fetch(PDO::FETCH_ASSOC);
	?>
	<div class="container">
		<div class="datupload z-depth-3 white animated bounceInUp">
			<form action ="controller/reviewer.php" method ="post">
				<h3>You are currently reviewing Print n°<?php  if (isset($_GET['id'])){
					echo $_GET['id'].' "'.$row['file'].'" by '.$theName['name']." ".$theName['surname']."!";
				} 
				
				?> </h3><br>

				<div class="row">
					<a class="btn btn-outline-blue col-md-6" title="Upload a file for review." href="<?php echo $row['link']; ?>">
						<p><br><i class="fa fa-cloud-download fa-5x" title="Upload a file for review."></i> Download File from google drive</p>
					</a>
					<div class="col-md">
						<div class="file-field">              
							<a class="btn-floating orange mt-0" title="Upload a file for review.">
								<i class="fa fa-cloud-upload" aria-hidden="true"  title="Upload a file for review."></i>
								<input type="file" Name="fileToUpload"  title="Upload a file for review.">
							</a>
							<div class="file-path-wrapper" title="Upload a file for review.">
								<input class="file-path validate" Name="fileToUpload" type="text" placeholder="Upload your file"  title="Upload a file for review.">
							</div><label class="myLabel"  title="Upload a file for review.">Gcode, OBJ and Stl files Only.</label>
						</div>
					</div>
				</div>
				
				<br>
				
				<div class="row">
					<div class="col-md">
						<select name="isGroup" class="mdb-select colorful-select dropdown-primary"  required>
							<option  value="" disabled selected>Choose an option</option>
							<?php if($row['isGroup']==0)
							{
								echo '<option value="0" selected>No</option>';
							}else{
								echo '<option value="0">No</option>';
							}?>
							<?php if($row['isGroup']==1)
							{
								echo '<option value="1" selected>Yes</option>';
							}else{
								echo '<option value="1">Yes</option>';
							}?>


						</select>
						<label>Group work?</label>
					</div>

					<div class="col-md">	
						<select name="isPersonal" class="mdb-select colorful-select dropdown-primary " required>
							<option  value="" disabled>Choose an Answer</option>
							<option value="-1">Personal</option>
							<?php if($row['isPersonal']==-1)
							{
								echo '<option value="-1">Formative</option>';
							}else{
								echo '<option value="-1" selected>Formative</option>';
							}?>

							<?php if($row['isPersonal']==0)
							{
								echo '<option value="0" selected>Summative</option>';
							}else{
								echo '<option value="0" >Summative</option>';
							}?>
						</select>
						<label> What type of project is it?</label>
					</div>
				</div>



				<div class="row">
					<div class="col-md">
						<div class="md-form">
							<i class="fa fa-gbp prefix"></i>
							<input type="text" id="form1" name="price" value="<?php echo $row['price']; ?>"class="form-control">
							<label for="form1" >Price</label>
						</div>
					</div>

					<div class="col-md">
						<select name="material" class="mdb-select colorful-select dropdown-primary " required>
							<option  value="" disabled>Choose your Material</option>
							<?php    
							foreach($array1 as $yatangaki) {
								if ($row['material'] == $result){
									echo '<option value="'.$yatangaki.'" selected>'.$yatangaki.'</option><br>';
								}else{
									echo '<option value="'.$yatangaki.'">'.$yatangaki.'</option><br>';
								}
							}
							?>
						</select>
						<label>Material</label>
					</div>
				</div>


				<div class="row">
					<div class="col-md">
						<select name="quality" class="mdb-select colorful-select dropdown-primary" required>
							<option value="" disabled>Choose your Quality</option>
							<?php if($row['quality']=='Extra Coarse')
							{
								echo '<option value="Extra Coarse" selected>Extra Coarse (0.6mm) </option>';
							}else{
								echo '<option value="Extra Coarse">Extra Coarse (0.6mm) </option>';
							}?>
							<?php if($row['quality']=='Coarse')
							{
								echo '<option value="Coarse" selected>Coarse (0.4mm) </option>';
							}else{
								echo '<option value="Coarse">Coarse (0.4mm) </option>';
							}?>
							<?php if($row['quality']=='Draft')
							{
								echo '<option value="Draft" selected>Draft (0.2mm)</option>';
							}else{
								echo '<option value="Draft">Draft (0.2mm)</option>';
							}?>
							<?php if($row['quality']=='Low')
							{
								echo '<option value="Low" selected>Low (0.15mm) </option>';
							}else{
								echo '<option value="Low">Low (0.15mm) </option>';
							}?>
							<?php if($row['quality']=='Fine')
							{
								echo '<option value="Fine" selected>Fine (0.1mm) </option>';
							}else{
								echo '<option value="Fine">Fine (0.1mm) </option>';
							}?>
							<?php if($row['quality']=='Fine')
							{
								echo '<option value="Awesome" selected>Extra Fine (0.06mm) </option>';
							}else{
								echo '<option value="Awesome">Extra Fine (0.06mm) </option>';
							}?>
						</select>
						<label>Quality</label>
					</div>

					<div class="col-md">				
						<select name="colour" class="mdb-select colorful-select dropdown-primary " required>
							<option value="" disabled>Choose your Colour</option>
							<?php    
							foreach($array as $result) {
								if ($row['colour'] == $result){
									echo '<option value="'.$result.'" selected>'.$result.'</option><br>';
								}else{
									echo '<option value="'.$result.'">'.$result.'</option><br>';
								}
							}
							?>
						</select>
						<label>Colour</label>
					</div>
				</div>


				<div class="row">
					<div class="col-md">
						<div class="md-form">
							<p>Comment: <?php							
							echo $row['comment'];
							?></p>

						</div>
					</div>
				</div>


				<div class="row">
					<button class="btn btn-rounded green col-md" name="id" value="<?php echo $_GET['id']; ?>">send payment email and review</button>
					<button class="btn btn-rounded red col-md" name="bad" value="<?php echo $_GET['id']; ?>"> Declare model as faulty</button>
				</div>
			</form>

		</div>
	</div>
	<script type="text/javascript">

		$(document).ready(function() {$('.mdb-select').material_select();});

	</script>

</body>
<?php
include('footer.php');
?>
</html>
