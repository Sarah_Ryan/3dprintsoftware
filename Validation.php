<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head> 
 <?php 
 require("controller/sessionstart.php"); 
 require('navbar.php');

 ?>

 <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
 <meta charset="UTF-8">

 <title>Validation</title>

</head>

<body class="white">



    <div class="container-fluid white dathomepage" height=100%>
      <!-- Content here -->

      <h2 class="txttweak animated slideInLeft"> Welcome <?php echo $_SESSION['name']; ?> to the 3D Printing Management System ! </h2> 
      <div class="row">
        <div class="col-md">
            <div class="row">
                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="NewPrint.php" method ="post">
                        <button class="btn btn-outline-deep-purple waves-light col-md" mdbRippleRadius> <img src="img/original-prusa-i3-mk2-1.jpg" class="img-fluid" alt="Card image with a tree in a middle of a lake."></img><br>New Print Job</button>
                    </form>
                </div>

                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="Current.php" method ="post" >

                        <button class="btn btn-outline-deep-purple waves-light col-md" mdbRippleRadius><img src="img/active.jpg" class="img-fluid" alt="Card image with a tree in a middle of a lake."></img><br>Current Print jobs</button>
                    </form>
                </div>

                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="Old.php" method ="post" >
                        <button class="btn btn-outline-deep-purple waves-light col-md" mdbRippleRadius><img src="img/old.jpg" class="img-fluid" alt="Card image with a tree in a middle of a lake."></img><br>Old Print Jobs</button>
                    </form>
                </div>
            </div>

            <?php  if($_SESSION['isAdmin']==1){  ?>

            <h2 class="txttweak animated slideInLeft"> SECRET ADMIN STUFF </h2>

            <div class="row">
                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="jobdeck.php" method ="post" >
                        <button class="btn btn-outline-red waves-light col-md" mdbRippleRadius><img src="img/admin.png" class="img-fluid"></img><br>Manage All Prints</button>   
                    </form>
                </div>
                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="adminOld.php" method ="post" > 
                        <button class="btn btn-outline-red waves-light col-md" mdbRippleRadius><img src="img/admin.png" class="img-fluid"></img><br>Old Print Jobs</button>       
                    </form>
                </div>
                <div class="col-md flex-center d-flex align-items-stretch">
                    <form action ="adminAccounts.php" method ="post" >          
                        <button class="btn btn-outline-red waves-light col-md" mdbRippleRadius><img src="img/admin.png" class="img-fluid"></img><br>Account Manager</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-3">

            <div class="row">
<!--                 <div class="col"></div>
                <div class="col"></div>   -->  
                <div class="col"> 
    <h5 class="txttweak animated slideInLeft">Printer status</h5>
                    <!--Card-->
                    
                    <div class="card">

                        <!--Card image-->
                        <div class="view overlay hm-white-slight">
                            <img src="http://www.prusaprinters.org/wp-content/uploads/2016/05/IMG_0785.jpg" class="img-fluid" alt="">
                            <a onclick="openOctoPi()">
                                <div class="mask"></div>
                            </a>
                        </div>

                        <!--Card content-->
                        <div class="card-body">
                            <!--Title-->
                            <p class="card-title">Prusa i3 MK2</p>
                            <!--Text-->
                            <p class="card-text">
                                <p style="font-size: 0.8rem;" id="datPrinter"></p>
                                <p style="font-size: 0.8rem;" id="datFile"></p>
                                <p style="font-size: 0.8rem;" id="datTime"></p>
                                <p style="font-size: 0.8rem;" id="datProgress"></p></p>
                                <div class="progress" id="datHiddenPB" >
                                    <div  id="datProgressBar" class="progress-bar bg-info" role="progressbar" style="width: 1%" aria-valuemin="0" aria-valuemax="100" ></div>
                                </div>
                                <a onclick="openOctoPi()" class="col btn btn-primary">Access Webcam</a>
                                <a onclick="stopPrint()" id="stopButton" class="col btn btn-red">STOP Print <i class="fa fa-times-circle"></i></a>
                                <a onclick="reconnectPrinter()" id="reconnectPrinter" class="col btn btn-red">Reconnect Printer <i class="fa fa-times-circle"></i></a>
                            </div>

                        </div>
                        <!--/.Card-->
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    


    <!-- MODALS -->
    <!-- MODALS -->
    <!-- MODALS -->
    <!-- MODALS -->
    <!-- MODALS -->
    <!-- MODALS -->


    <!-- Central Modal Medium Success -->
    <div class="modal fade" id="connectionFailed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Connection Timed out </p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>
                <   
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-warning fa-4x mb-3 animated rotateIn"></i>
                        <p>The connection to the Printer has been lost.</p>
                        <p style="text-align: left;">Make sure to check if it's still online. In the case of a printing error, follow the instructions on its screen and make sure to recalibrate it before attempting to reconnect.</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">OK</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->


    <!-- Central Modal Medium Success -->
    <div class="modal fade" id="passchange" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-success" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Password </p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>
                <   
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-lock fa-4x mb-3 animated rotateIn"></i>
                        <p>You password has been changed succesfully.</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">OK</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->



    <?php if(isset($_GET['passchange']) == 'yay'){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#passchange").modal('show');});
    </script>
    <?php } ?>

    <!-- Central Modal Medium Success -->
    <div class="modal fade" id="printSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-success" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Upload Success</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>
                        <p><?php echo $_SESSION['name']; ?> !<br>Your model "<?php echo $_GET['modelName']; ?>" has been uploaded successfully you'll get notified as well by email of its completion.</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">

                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Thanks</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->

    <?php if(isset($_GET['print']) == true){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#printSuccess").modal('show');});
    </script>
    <?php } ?>




</body>
<?php 
include('footer.php');
?>
</html>
