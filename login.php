<!DOCTYPE html>
    <!--
    To change this license header, choose License Headers in Project Properties.
    To change this template file, choose Tools | Templates
    and open the template in the editor.
-->





<html>
<head>
    <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
    <meta charset="UTF-8">
    <title>Login </title>
    <?php 
    include("header.php");
    ?>
</head>

<body>
    <form action="controller/logger.php" method="post">
        <div class="container">
            <div class="containersamere z-depth-3">
              <!-- Content here -->

               <h2 class="txtlogin"> Please login </h2>

            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md">

                    <div class="md-form">
                        <i class="fa fa-envelope prefix"></i>
                        <input type="text" id="form9" class="form-control " name="username" required >
                        <label for="form9">Type your email</label>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md">               
                    <div class="md-form">
                        <i class="fa fa-lock prefix"></i>
                        <input type="password" id="form10" class="form-control " name="pass" required>
                        <label for="form10">Type your password</label>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
<!--             <div class="row">
                <div class="col-md-2"></div>     
                <div class="col-md-8 g-signin2 " data-onsuccess="onSignIn"></div>
                <div class="col-md-2"></div>
            </div> --> 
            <div class="row">
                <div class="col-md-2"></div>
                <button class="btn btn-deep-purple deep-purple accent-3 waves-light   col-md" mdbRippleRadius>Login</button>
                <div class="col-md-2"></div>
            </div>
            <div class="row"><br></div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md">
                    <a class="text-center" href="registration.php"><p>I don't have an account !</p></a>
                    <a class="text-center" href="recovery.php"><p>Forgot Password?</p></a>
                </div>
                <div class="col-md-2"></div>
            </div>
        </div> 
    </div>
</form> 

<script type="text/javascript">
    function onSignIn(googleUser){

        var profile=googleUser.getBasicProfile();
/*    $(".BeforeLogin").css("display","none");
    $(".data").css("display","block");
    $(".signinpls").css("display","none");
    $("#pic").attr('src',profile.getImageUrl());
    $("#email").text(profile.getEmail());
    $("#email2").text(profile.getEmail());*/
    gapi.auth.authorize(
    {
      client_id: clientId, 
      scope: scopes, 
      immediate: false, 
      cookie_policy: 'single_host_origin'
  },
  handleAuthResult);

}

/* When user logout with google */
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function(){
        alert("you have been successfully signed out");
        $(".BeforeLogin").css("display","block");
        $(".data").css("display","none");
        $(".requestit").css("display", "none");
    });
    gapi.auth2.getAuthInstance().disconnect();
    window.location.reload()
    
}
</script>

               

    <!-- Central Modal Medium Danger -->
    <div class="modal fade" id="lockedAcc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-danger" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">ACCOUNT LOCKED</p>
    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
    
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa  fa-lock fa-4x mb-3 animated rotateIn"></i>
                        <p>Your account is now locked after 3 unsuccessfull login attempts.</p>
                    </div>
                </div>
    
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Okay</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Danger-->
    
    <?php 
    if (isset($_GET['locked']) == true) { ?>
        <script type="text/javascript">
        $(document).ready(function(){$("#lockedAcc").modal('show');});
         </script>
<?php } ?>                                
    
    <!-- Central Modal Medium Warning -->
    <div class="modal fade" id="wrongPass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-warning" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Wrong Password</p>
    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
    
                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa  fa-exclamation-triangle fa-4x mb -3 animated rotateIn"></i>
                        <p>WRONG PASSWORD!<br> Try Again! <br> Failing to  provide the right password will result in having your account LOCKED. So pay attention!</p>
                    </div>
                </div>
    
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Alright, Thanks</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Warning-->

<?php if(isset($_GET['wrongPass']) == true){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#wrongPass").modal('show');});
    </script>
<?php } ?>


   
    <!-- Central Modal Medium Success -->
    <div class="modal fade" id="passreset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-notify modal-success" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <p class="heading lead">Yay</p>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="white-text">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">
                    <div class="text-center">
                        <i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>
                        <p>Dear <?php echo $_GET['name']; ?>,<br> Your Password has been reset. Make sure to check your inbox <br>(<?php echo $_GET['email'] ?>) to get your new temporary password.</p>
                    </div>
                </div>

                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Thanks</a>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Central Modal Medium Success-->

<?php if(isset($_GET['passreset']) == true){ ?>
    <script type="text/javascript">
        $(document).ready(function(){$("#passreset").modal('show');});
    </script>
<?php } ?>


</body>

<?php 
include("footer.php");
?>

</html>