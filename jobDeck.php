<!DOCTYPE html>
<html>
<head>
	<title>The Job Deck</title>
	<?php 
	include('controller/sessionstart.php');
	include('navbar.php');
	include('controller/db.php');
	?>
</head>
<body>

	<h2 class="txttweak">Job List</h2>
	<div class="container-fluid">

		<div class="row">
			<div class="col">

				<div class="row border border-dark z-depth-3">
					<div class="col-lg">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr><th>To slice  <?php $db = DBconnection();
									$req = $db->query("SELECT * FROM prints WHERE isApproved=0 ORDER BY date ASC, priority DESC");
									if($req->rowCount() > 0){
										$countToSlice = $req->rowCount();  

										if ($countToSlice==0 ){

										}else{
											echo'<span class="badge red" style="border-radius:10px;">'.$countToSlice.'</span>';
										}
										?>	
									</th></tr>
								</thead>
								<tbody>
									<?php 
									/*			if($req->rowCount() > 0){*/
										while($row = $req->fetch(PDO::FETCH_ASSOC)){ ?>
										<tr><td><a href="review.php?id=<?php echo $row['printID']; ?>"><?php echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].'</span>';?></a></td></tr>
										<?php } } $req->closecursor(); ?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-lg">
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr><th>To Pay <?php 
										$req = $db->query("SELECT * FROM prints WHERE isPaid=0 AND isApproved ORDER BY date ASC, priority DESC  ");  
										$countToPay = $req->rowCount();  

										if ($countToPay==0 ){

										}else{
											echo'<span class="badge red" style="border-radius:10px;">'.$countToPay.'</span>';
										}?>	

									</th></tr>
								</thead>
								<tbody>
									<?php if($req->rowCount() > 0){ 
										while($row = $req->fetch(PDO::FETCH_ASSOC)){ ?>
										<tr><td><?php
										echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].'</span>'; ?></td></tr>
										<?php } }
										$req->closecursor();
										?>
									</tbody>
								</table>
							</div>
						</div>

						<div class="col-lg">
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr><th>Wait for sorting
											<?php 
											$req = $db->query("SELECT * FROM prints WHERE isApproved=1 AND isPaid=1 AND date>= ".date('Y-m-d')."  ORDER BY date ASC, priority DESC  ");
											$countToPay = $req->rowCount();  

											if ($countToPay==0 ){

											}else{
												echo'<span class="badge red" style="border-radius:10px;">'.$countToPay.'</span>'; 
											}?></th></tr>
										</thead>
										<tbody>
											<?php if($req->rowCount() > 0){
												?>
												<?php 
												while($row = $req->fetch(PDO::FETCH_ASSOC)){
													?>
													<tr><td><?php echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].'</span>'; ?></td></tr>
													<?php } }
													$req->closecursor();
													?>
												</tbody>
											</table>
										</div>
									</div>

									<div class="col-lg">
										<div class="table-responsive">
											<table class="table table-bordered">
												<thead>
													<tr><th>Print stack <?php 
													$req = $db->query("SELECT * FROM prints WHERE isApproved=1 AND isPaid=1  ORDER BY date ASC, priority DESC  ");
													$countToPay = $req->rowCount();  

													if ($countToPay==0 ){

													}else{
														echo'<span class="badge red" style="border-radius:10px;">'.$countToPay.'</span>'; 
													} ?></th></tr>
												</thead>
												<tbody>

													<?php if($req->rowCount() > 0){
														?>
														<?php 
														while($row = $req->fetch(PDO::FETCH_ASSOC)){
															?>
															<tr><td><?php echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].'</span>'; ?></td></tr>
															<?php } }
															$req->closecursor();
															?>
														</tbody>
													</table>
												</div>
											</div>

											<div class="col-lg">
												<div class="table-responsive">
													<table class="table table-bordered">
														<thead>
															<tr><th>Currently printing <?php 

															$req = $db->query("SELECT * FROM prints WHERE isApproved=1 AND isPrinting=1 ORDER BY date ASC, priority DESC  "); 
															$countToPay = $req->rowCount();  

															if ($countToPay==0 ){

															}else{
																echo'<span class="badge red" style="border-radius:10px;">'.$countToPay.'</span>'; 
															}?></th></tr>
														</thead>
														<tbody>

															<?php if($req->rowCount() > 0){
																?>
																<?php 
																while($row = $req->fetch(PDO::FETCH_ASSOC)){
																	?>
																	<tr><td><?php echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].' </span>'; ?></td></tr>
																	<?php } }
																	$req->closecursor();
																	?>
																</tbody>
															</table>
														</div>
													</div>

													<div class="col-lg">
														<div class="table-responsive">
															<table class="table table-bordered">
																<thead>
																	<tr><th>Pick up <?php 

																	$req = $db->query("SELECT * FROM prints WHERE isPrinted=1 AND isPickedUp=0 ORDER BY date ASC, priority DESC  "); 
																	$countToPay = $req->rowCount();  

																	if ($countToPay==0 ){

																	}else{
																		echo'<span class="badge red" style="border-radius:10px;">'.$countToPay.'</span>'; 
																	}?></th></tr>
																</thead>
																<tbody>

																	<?php if($req->rowCount() > 0){
																		?>
																		<?php 
																		while($row = $req->fetch(PDO::FETCH_ASSOC)){
																			?>
																			<tr><td><?php echo $row['file'].' <span class="badge blue" style="border-radius:10px;">'.$row['priority'].'</span>'; ?></td></tr>
																			<?php } }
																			$req->closecursor();
																			?>
																		</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-3">
														<p class="txttweak animated slideInRight">Printer status</p>
														<!--Card-->
														<div class="card">

															<!--Card image-->
															<div class="view overlay hm-white-slight">
																<img src="http://www.prusaprinters.org/wp-content/uploads/2016/05/IMG_0785.jpg" class="img-fluid" alt="">
																<a onclick="openOctoPi()">
																	<div class="mask"></div>
																</a>
															</div>

															<!--Card content-->
															<div class="card-body">
																<!--Title-->
																<p class="card-title">Prusa i3 MK2</p>
																<!--Text-->
																<p class="card-text">
																	<p style="font-size: 0.8rem;" id="datPrinter"></p>
																	<p style="font-size: 0.8rem;" id="datFile"></p>
																	<p style="font-size: 0.8rem;" id="datTime"></p>
																	<p style="font-size: 0.8rem;" id="datProgress"></p></p>
																	<div class="progress" id="datHiddenPB" >
																		<div  id="datProgressBar" class="progress-bar bg-info" role="progressbar" style="width: 1%" aria-valuemin="0" aria-valuemax="100" ></div>
																	</div>
																	<a onclick="openOctoPi()" class="col btn btn-primary">Access Webcam</a>
																	<a onclick="stopPrint()" id="stopButton" class="col btn btn-red">STOP Print <i class="fa fa-times-circle"></i></a>
																	<a onclick="reconnectPrinter()" id="reconnectPrinter" class="col btn btn-red">Reconnect Printer <i class="fa fa-times-circle"></i></a>
																</div>
															</div>
															<!--/.Card-->
														</div>

													</div>
												</div>
												<?php include('footer.php');?>
											</body>
											</html>