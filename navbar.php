<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">


	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="css/mdb.min.css" rel="stylesheet">
	<!-- Your custom styles (optional) -->

	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.css">

	<!-- datatable -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

</head>
<header>
	<!--Navbar-->
	<nav class="navbar navbar-expand-lg navbar-dark  blue accent-2">
		
		<!-- Collapse button -->
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
				aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span> <span class="badge red count" style="border-radius:10px;"></span></button>

		<!-- Navbar brand -->
		<a class="navbar-brand" href="Validation.php"><img src="img/uopdark.png" height="30" alt="Responsive image"> EPC3DPMS</a>

		
		<!-- Collapsible content -->
		<div class="collapse navbar-collapse" id="navbarSupportedContent">

			<!-- Links -->
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"> 
					<a class="nav-link" href="Validation.php">Home
					</a>
				</li>

				<li class="nav-item dropdown ">

					<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">New Print</a>

					<div class="dropdown-menu dropdown-menu-left dropdown-primary" aria-labelledby="navbarDropdownMenuLink-4">

						<?php  if($_SESSION['isAdmin']==0){  ?>

						<a class="dropdown-item waves-effect waves-light" href="NewPrint.php">Filament printers</a>

						<?php  }else{  ?>

						<a class="dropdown-item waves-effect waves-light green" href="NewPrint.php">Fillament printers
						</a>

						<?php } ?>
					</div>

				</li>

				
				<li class="nav-item">
					<a class="nav-link" href="Current.php">Current Prints
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="Old.php">Old Prints
					</a>
				</li>


				<?php  if($_SESSION['isAdmin']==1){  ?>
				<li class="nav-item red">
					<a class="nav-link" href="jobDeck.php">All Prints
					</a>
				</li>
				<li class="nav-item red">
					<a class="nav-link" href="adminOld.php">Old Jobs
					</a>
				</li>
				<li class="nav-item red">
					<a class="nav-link" href="adminAccounts.php">UserList
					</a>
				</li>
				<!-- Dropdown -->
				<!-- <li class="nav-item red dropdown">
					<a class="nav-link red dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Admin Stuff</a>
					<div class="dropdown-menu dropdown-danger" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="jobDeck.php">Print Job Management</a>
						<a class="dropdown-item" href="adminOld.php">All Old Jobs</a>
						<a class="dropdown-item" href="adminAccounts.php">User List editor</a>
					</div>
				</li> -->
				<?php } ?>
			</ul>
			<!-- Links -->
			<ul class="navbar-nav ml-auto">

				<li class="nav-item dropdown ">

					<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i id="datBell" class="fa fa-bell"><span class="badge red count" style="border-radius:10px;"></span></i></a>

					<div class="dropdown-menu dropdown-menu-right dropdown-primary ellipsis notif" aria-labelledby="navbarDropdownMenuLink-4">

					</div>

				</li>



				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i  class="fa fa-user" href="#"></i><?php echo $_SESSION['name']; if ($_SESSION['isPowerUser']==1){ echo '<span class="badge green" style="border-radius:20px;">P</span>';} ?></a>
					<div class="dropdown-menu dropdown-menu-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink-4">
						<!-- <a class="dropdown-item waves-effect waves-light" href="javascript:;;">My account</a> -->
						<a class="dropdown-item waves-effect waves-light" href="changePass.php">Change Password</a>
						<a class="dropdown-item waves-effect waves-light" href="logout.php">Log out</a>
					</div>
				</li>
			</ul>
	</nav>
	<!--/.Navbar--> 



	<!-- Central Modal Medium Info -->
	<div class="modal fade" id="openMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-notify modal-info" role="document">
			<!--Content-->
			<div class="modal-content">
				<!--Header-->
				<div class="modal-header">
					<p class="heading lead" id="messageTitle"></p>

					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<!--Body-->
				<div class="modal-body">
					<div class="text-center">
						<i class="fa fa-envelope-o fa-4x mb-3 animated rotateIn"></i>

						<p id="message"></p>
					</div>
				</div>

				<!--Footer-->
				<div class="modal-footer justify-content-center">
					<!-- <a type="button" class="btn btn-primary-modal">Get it now <i class="fa fa-diamond ml-1"></i></a> -->
					<a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">close</a>
				</div>
			</div>
			<!--/.Content-->
		</div>
	</div>
	<!-- Central Modal Medium Info-->

 
  <!-- Central Modal Medium Success -->
  <div class="modal fade" id="openOctoPi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog modal-notify modal-info" role="image">
  		<!--Content-->
  		<div class="modal-content">
  			<!--Header-->
  			<div class="modal-header">
  				<p class="heading lead">Webcam</p>

  				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
  					<span class="white-text">&times;</span>
  				</button>
  			</div>

  			<!--Body-->
  			<div class="modal-body">
  				<div class="text-center">
  					<img src="http://10.128.121.184/webcam/?action=stream" style="width:100%;"></img>
  				</div>
  			</div>

  			<!--Footer-->
  			<div class="modal-footer justify-content-center">

  				<a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Close</a>
  			</div>
  		</div>
  		<!--/.Content-->
  	</div>
  </div>
  <!-- Central Modal Medium Success-->


	<script type="text/javascript">

		function openOctoPi()
		{
			$("#openOctoPi").modal('show');
		}

		function stopPrint()
		{
			$.ajax({
				  url:"stopPrint.php",
				  method:"GET",
				dataType:"json",
				  success:function(data)
				  {

				}
			});

		}

		function reconnectPrinter()
		{
			$.ajax({
				  url:"reconnectPrinter.php",
				  method:"GET",
				dataType:"json",
				  success:function(data)
				  {

				}
			});

		}
	</script>


	<script>
		var errorShown;
		$(document).ready(function(){

// updating the view with notifications using ajax

function getStatus(view = '')

{ 
	$.ajax({
		  url:"fetchStatus.php",
		  method:"GET",
		dataType:"json",
		  success:function(data)
		  {
			if (errorShown==1)
			{
				errorShown=0;
			}
			
			$('#datPrinter').text('Satus: '+ data.state); 

			if (data.state=="Printing")
			{
				$('#datFile').text('File Name: '+ data.job.file.name);
				$('#stopButton').show();
				if (data.progress.printTimeLeft == null)
				{
					$('#datTime').text('Time left: Stabilizing...');
				}
				else
				{
					var time=secondsToHms(data.progress.printTimeLeft);
					$('#datTime').text('Time left: '+ time);
				}
				$('#datHiddenPB').show();
				$('#datProgress').text('Progress: ' + Math.floor((data.progress.completion / 100) * 100)+'%');
				$('#datProgressBar').width(data.progress.completion+'%');
			}
			else if (data.state.indexOf("Offline") !== -1)
			{
				$('#stopButton').hide();
				$('#datHiddenPB').hide();
				$('#datFile').text('');
				$('#datTime').text('');
				$('#datProgress').text('');
				$('#datReconnectButton').show();
			}
			else
			{
				$('#stopButton').hide();
				$('#datHiddenPB').hide();
				$('#datFile').text('');
				$('#datTime').text('');
				$('#datProgress').text('');
			}


		},
		error: function(xhr, textStatus, errorThrown)
		{
			$('#datHiddenPB').hide();

			$('#datPrinter').text('Server UNREACHABLE'); 
			$('#datFile').text('');
			$('#datTime').text('');
			$('#datProgress').text('');
			if (errorShown==0){
				$("#connectionFailed").modal('show');
				errorShown=1;
			}

		}   
	 });

}

function secondsToHms(d) 
{
	d = Number(d);
	var h = Math.floor(d / 3600);
	var m = Math.floor(d % 3600 / 60);
	var s = Math.floor(d % 3600 % 60);

	var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
	var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
	var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
	return hDisplay + mDisplay + sDisplay; 
}
getStatus();


setInterval(function(){getStatus();;}, 5000);
});


</script>


<script>
	
	var messageTitle,message;
	$(document).ready(function()
	{

// updating the view with notifications using ajax

function load_unseen_notification(view = '')

{
	$.ajax({
		  url:"fetch.php",
		  method:"POST",
		  data:{view:view},
		  dataType:"json",
		  success:function(data)
		  {
			messageTitle=data.messageTitle;
			message = data.message;

			$('.notif').html(data.notification);

			   if(data.unseen_notification > 0)
			   {
				    $('.count').html(data.unseen_notification);
			   }
		  }
	 });

}

load_unseen_notification();


// load new notifications

$(document).on('click', '.notif', function()
{

	 $('.count').html('');

	 load_unseen_notification('yes');

});

setInterval(function(){load_unseen_notification();;}, 5000);
});
	function openMessage(id)
	{
		$("#openMessage").modal('show');
		document.getElementById("messageTitle").innerHTML = messageTitle[id];
		document.getElementById("message").innerHTML = message[id];
	}

</script>

</header>
