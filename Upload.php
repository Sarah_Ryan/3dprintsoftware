<?php

session_start();
require('controller/db.php');
require('googleWrap.php');
include('insert.php');

//specifies directory for file
$target_dir = "uploads/";
//path of file to be uploaded
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
//Check variable
$uploadOK = 1;
//Holds file extension
$fileType = pathinfo($target_file,PATHINFO_EXTENSION);
//checks if real image
if(isset($_POST["submit"])){
  $uploadOK = 1;
}
//Allows certain file types
if($fileType != "gcode" && $fileType != "obj" && $fileType != "stl"){
  echo "only gcodes and 3D models are allowed";
  $uploadOK = 0;
}

if($uploadOK == 0){
  echo "Sorry, file not uploaded";
}else{
}
if (strpos($_SESSION['username'], '@myport.ac.uk') !== false) {
  echo 't';
  $datName =str_ireplace('@myport.ac.uk','', $_SESSION['username']).'_'.date("dmygis").'.'.pathinfo($target_file,PATHINFO_EXTENSION);
  $username = str_ireplace('@myport.ac.uk','', $_SESSION['username']);
}else{
  $datName = $_SESSION['username'].'_'.date("dmygis").'.'.pathinfo($target_file,PATHINFO_EXTENSION);
  $username = $_SESSION['username'];
}
/*$url = "http://169.254.80.217/api/files/local";
$post = array('file' => new CurlFile($_FILES["fileToUpload"]["tmp_name"],$_FILES["fileToUpload"]["type"],$datName),
  'path' => $username,
  'print' => 'true'
);
$ch = curl_init();
curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Api-Key: 45B1A7CDD72949EAAD069F5FCC4F463F'));
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
$result=curl_exec ($ch);
curl_close ($ch);
*/

$client = getClient();
$service = new Google_Service_Drive($client);
$fileMetadata = new Google_Service_Drive_DriveFile(array(
  'name' => $datName,
  'parents' => array('0Bwhrcys-IU5zT3F6UURjQXlPMzQ')
));

$content = file_get_contents($_FILES["fileToUpload"]["tmp_name"]);

$file = $service->files->create($fileMetadata, array(
  'data' => $content,
  'mimeType' => 'text/plain',
  'uploadType' => 'multipart',
  'fields' => 'id'));
$datID = $file->id;
printf("File ID: %s\n",$datID);
$driveService = new Google_Service_Drive($client);
$fileId = $datID;
$response = $driveService->files->get($fileId, array(
  'alt' => 'media'));
$content = $response->getBody()->getContents();
$db = DBconnection();
$priority=$_SESSION['priority']+$_POST['isPersonal']+$_POST['isGroup'];




if($fileType == "gcode"){
  include('filament_length.php'); 
  $cost =  getCost(1.75, 1.25,$_FILES["fileToUpload"]["tmp_name"]);
  echo $cost;
}else{
  $cost=0;
}

$comment = $_POST['comment'];

$req = $db->query("INSERT INTO prints (userID,date,file,gdFile,material,quality,color,priority,price,link,comment) VALUES ('".$_SESSION['userID']."','".date("Y-m-d")."','".$_FILES['fileToUpload']['name']."','".$datName."','".$_POST['material']."','".$_POST['quality']."','".$_POST['colour']."','".$priority."','".$cost."','https://drive.google.com/open?id=".$datID."','".$comment."')");
$req->closeCursor();


if ($req == null){
  echo "<h2>Yikes!</h2><p>Something went wrong...</p><a href='../test/index.php'><br><p>Go Back</p></a>";

}else{
  
  echo "<h2>Yay</h2><p>New Record Written successfully</p>";

  $comments = 'Your model "'.$_FILES['fileToUpload']['name'].'" has been uploaded successfully you &rsquo; ll get notified as well by email of its completion.';
  $subject = 'Upload Success';
  $destination = $_SESSION['userID'];
  sendNotif($destination, $subject , $comments);
  
  if ($fileType == "obj" || $fileType == "stl"){
    $comments = $_SESSION['name'].' '.$_SESSION['surname'].'&rsquo; s model "'.$_FILES['fileToUpload']['name'].'.'.pathinfo($target_file,PATHINFO_EXTENSION).'" has been uploaded successfully. The Model Requires your attention. <a type="button" class="btn btn-outline-secondary-modal waves-effect" href="https://drive.google.com/open?id='.$datID.'">click here to download</a>';
    $subject = 'New print Job';
    $destination = 'ADMIN';
    sendNotif($destination, $subject , $comments);
  }else{
    $comments = $_SESSION['name'].' '.$_SESSION['surname'].'&rsquo; s model "'.$_FILES['fileToUpload']['name'].'.'.pathinfo($target_file,PATHINFO_EXTENSION).'" has been uploaded successfully.';
    $subject = 'New print Job';
    $destination = 'ADMIN';
    sendNotif($destination, $subject , $comments);
  }

  header('Location: Validation.php?print=true&&modelName='.$_FILES['fileToUpload']['name']);
}
?>