<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
<head>
  <?php 
  require("/controller/sessionstart.php");
  require('../test/controller/db.php'); 
  include("navbar.php");
  ?>
  <!-- <link rel ="stylesheet" type ="text/css" href="LoginStyle.css"> -->
  <meta charset="UTF-8">
  <title>List of Old Jobs for <?php echo $_SESSION['username']; ?></title>

  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>

  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

</head>
<body>
<form action ="controller/old.php" method ="post" >
  <div class="container">

    <h2 class="text-center txttweak"> Old Print Jobs for <?php echo $_SESSION['name']; ?> <?php echo $_SESSION['surname']; ?></h2>
    <div class="row">

      <div class="col-md">
        <?php
        $db = DBconnection();
        $req = $db->query("SELECT * FROM prints WHERE userID='".$_SESSION['userID']."' AND isPrinted=1");
        $message=0;
        ?>

        <?php if($req->rowCount() > 0): 
        $message=1;?>
        <!--  <div class="animated slideInLeft"> -->
          <table id="table" class="table table-hover table-striped table-bordered table-responsive w-auto" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th >Select </th>
                <th >#<i class="fa fa-sort ml-1"></i></th>
                <th>Date<i class="fa fa-sort ml-1"></i> </th>
                <th>File<i class="fa fa-sort ml-1"></i> </th>
                <th>Material<i class="fa fa-sort ml-1"></i> </th>
                <th>Quality<i class="fa fa-sort ml-1"></i> </th>
                <th>Color<i class="fa fa-sort ml-1"></i> </th>
                <tr>
                </thead>
                <tbody>
                  <?php
                  $printNum = 1; 
                  while($row = $req->fetch(PDO::FETCH_ASSOC)): ?>
                  <tr>
                    <td><input type="checkbox" class="filled-in deep-purple accent-3" id="<?php echo $row['printID']; ?>" name="gaben[]" value="<?php echo $row['printID']; ?>">
                      <label for="<?php echo $row['printID']; ?>"></label></td>
                      <th scope="row"><?php echo $printNum; ?></th>
                      <td><?php echo $row['date']; ?></td>
                      <td><?php echo $row['file']; ?></td>
                      <td><?php echo $row['material']; ?></td>
                      <td><?php echo $row['quality']; ?></td>
                      <td><?php echo $row['color']; ?></td>
                    </tr>
                    <?php
                    $printNum++;
                  endwhile; 
                  $req->closecursor();?>
                </tbody>
              </table>
              <!--               </div>  -->        
            </div>
            <div class="col-md animated slideInUp flex-center">
              <?php if ($message==1){ ?>
              <button class="btn btn-primary deep-purple accent-3 waves-light" mdbRippleRadius value="1">Set to reprint</button>
              <?php } ?>
            </div>
          <?php endif; ?>
          <?php if ($message==0){ ?>
          <h2 class="text-center txttweak">YOU HAVE NO OLD JOBS</h2>
          <?php } 
          include("footer.php");
          ?>

        </div>
      </div>
    </form>
  </body>
  </html>
